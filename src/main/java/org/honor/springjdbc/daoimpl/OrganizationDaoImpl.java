package org.honor.springjdbc.daoimpl;

import java.util.List;

import javax.sql.DataSource;

import org.honor.springjdbc.dao.OrganizationDao;
import org.honor.springjdbc.domain.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class OrganizationDaoImpl implements OrganizationDao {
	
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	public boolean create(Organization org) {
		String query = "insert into organization (company_name, year_of_incorporation, postal_code, employee_count, slogan) "
				+ "values (:companyName,:yearOfIncorporation,:postalCode,:employeeCount,:slogan)";
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(org);
		return namedParameterJdbcTemplate.update(query, beanParams) == 1;
		/*Object[] args = new Object[] {org.getCompanyName(), org.getYearOfIncorporation(), org.getPostalCode(), org.getEmployeeCount(), org.getSlogan()};
		return jdbcTemplate.update(query, args) == 1;*/
	}

	public Organization getOrganization(int id) {
		String sqlQuery = "select id, company_name, year_of_incorporation, postal_code, employee_count, slogan " +
				"from organization where id = :ID";
		SqlParameterSource params = new MapSqlParameterSource("ID", id);
		Organization org = namedParameterJdbcTemplate.queryForObject(sqlQuery, params, new OrganizationRowMapper());
		return org;
	}

	public List<Organization> getAllOrganizations() {
		String sqlQuery = "select * from organization";
		List<Organization> orgList = namedParameterJdbcTemplate.query(sqlQuery, new OrganizationRowMapper());
		return orgList;
	}

	public boolean delete(Organization org) {
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(org);
		String sqlQuery = "delete from organization where id = :id";
		//Object[] args = new Object[] {org.getId()};
		return namedParameterJdbcTemplate.update(sqlQuery, beanParams) == 1;
	}

	public boolean update(Organization org) {
		String sqlQuery = "update organization set slogan = :slogan where id = :id";
		SqlParameterSource beanParams = new BeanPropertySqlParameterSource(org);
		return namedParameterJdbcTemplate.update(sqlQuery, beanParams) == 1;
	}

	//this is not something you would normally expect in a Dao implementation. this is just for cleanup/testing purposes.
	public void cleanup() {
		String query = "truncate table organization";
		namedParameterJdbcTemplate.getJdbcOperations().execute(query);

	}

}
